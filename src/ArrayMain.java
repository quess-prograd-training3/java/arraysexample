import com.example.Array;

public class ArrayMain {
    public static void main(String[] args) {
        Array array=new Array();
        array.initializeArray();
        if(array.checkArrayType()==1){
            System.out.println("Even Array");
        }
        else if(array.checkArrayType()==2){
            System.out.println("Odd Array");
        }
        else if (array.checkArrayType()==3) {
            System.out.println("Mixed Array");
        }
    }
}
